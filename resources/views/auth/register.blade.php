@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="login-wrapper row">
            <div id="login" class="login loginpage col-lg-offset-2 col-md-offset-3 col-sm-offset-3 col-xs-offset-0 col-xs-12 col-sm-6 col-lg-8">    
                <div class="login-form-header">
                     <img src="{{ asset('data/icons/signup.png') }}" alt="login-icon" style="max-width:64px">
                     <div class="login-header">
                         <h4 class="bold color-white">Signup Now!</h4>
                         <h4><small>Please enter your data to register.</small></h4>
                     </div>
                </div>
               
                <div class="box login">

                    <div class="content-body" style="padding-top:30px">

                        <form id="msg_validate" action="{{ route('register') }}" novalidate="novalidate" class="no-mb no-mt">
                            <div class="row">
                                <div class="col-xs-12">

                                    <div class="col-lg-6 no-pl">
                                        <div class="form-group">
                                            <label class="form-label">Username</label>
                                            <div class="controls">
                                                <input type="text" class="form-control" name="username" placeholder="Mr. Yu Wan">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 no-pr">
                                        <div class="form-group">
                                            <label class="form-label">Phone</label>
                                            <div class="controls">
                                                <input type="text" class="form-control" name="phone" placeholder="09-9878654321">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 no-pl">
                                        <div class="form-group">
                                            <label class="form-label">Password</label>
                                            <div class="controls">
                                                <input type="password" class="form-control" name="password1" placeholder="12345678">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 no-pr">
                                        <div class="form-group">
                                            <label class="form-label">Repeat Password</label>
                                            <div class="controls">
                                                <input type="password" class="form-control" name="password2" placeholder="12345678">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="pull-left">
                                        <a href="{{ route('register') }}" class="btn btn-primary mt-10 btn-corner right-15">Sign up</a>
                                        <a href="{{ route('login') }}" class="btn mt-10 btn-corner signup">Login</a>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <p id="nav">
                    <a class="pull-left" href="#" title="Password Lost and Found">Forgot password?</a>
                    <a class="pull-right" href="ui-login.html" title="Sign Up">Login</a>
                </p>

            </div>
        </div>
    </div>
    @endsection