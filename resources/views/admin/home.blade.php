@extends('layouts.admin')

@section('content')

<div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats one">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <img src="{{ asset('data/app-images/icon-1.png') }}" alt="">
                            </div>
                            <p class="card-category">App Downloads </p>
                            <h3 class="card-title">123,65K</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="fa fa-gear"></i> App Downloads / Sales
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats two">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <img src="{{ asset('data/app-images/icon-2.png') }}" alt="">
                            </div>
                            <p class="card-category">Server Load </p>
                            <h3 class="card-title">45.78%</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="fa fa-bookmark-o"></i> Server load time
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats three">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <img src="{{ asset('data/app-images/icon-3.png') }}" alt="">
                            </div>
                            <p class="card-category">Total Sales</p>
                            <h3 class="card-title">$34,245</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="fa fa-calendar-o"></i> Last 24 Hours
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats four">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <img src="{{ asset('data/app-images/icon-4.png') }}" alt="">
                            </div>
                            <p class="card-category">Traffic this month</p>
                            <h3 class="card-title">78.34 Gb</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="fa fa-clock-o"></i> Just Updated
                            </div>
                        </div>
                    </div>
                </div>
                

                <div class="clearfix"></div>

                <div class="col-lg-7">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Total Sales Earnings</h2>
                            <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="morris-area-chart"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-lg-5">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="ref-num-box statistics-box text-center mt-15">
                                <div class="mb-10">
                                    <img src="{{ asset('data/app-images/f1.png') }}" class="ico-icon-o mt-10 mb-10" alt="">
                                    <h3 class="bold mb-10">31,053</h3>
                                    <p class="mb-10">Total Followers</p>
                                    <p class="mb-0 green-text">4.98%<i class="complete fa fa-arrow-up ml-10"></i></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="ref-num-box statistics-box text-center mt-15">
                                <div class="mb-10">
                                    <img src="{{ asset('data/app-images/f2.png') }}" class="ico-icon-o mt-10 mb-10" alt="">
                                    <h3 class="bold mb-10">23.6K</h3>
                                    <p class="mb-10">Users Impression</p>
                                    <p class="mb-0 green-text">7.62%<i class="complete fa fa-arrow-up ml-10"></i></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="ref-num-box statistics-box text-center mt-15">
                                <div class="mb-10">
                                    <img src="{{ asset('data/app-images/f3.png') }}" class="ico-icon-o mt-10 mb-10" alt="">
                                    <h3 class="bold mb-10">33.43K</h3>
                                    <p class="mb-10">Ads Reach</p>
                                    <p class="mb-0 green-text">4.98%<i class="complete fa fa-arrow-up ml-10"></i></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="ref-num-box statistics-box text-center mt-15">
                                <div class="mb-10">
                                    <img src="{{ asset('data/app-images/f4.png') }}" class="ico-icon-o mt-10 mb-10" alt="">
                                    <h3 class="bold mb-10">27.3%</h3>
                                    <p class="mb-10">Engagement Rate</p>
                                    <p class="mb-0 red-text">6.34%<i class="cancelled fa fa-arrow-down ml-10"></i></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="col-lg-6 col-xs-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Register / Purchases Chart</h2>
                            <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="morris_line_graph"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-lg-6 col-xs-12">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Orders Statistics</h2>
                            <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="relative">
                                        <canvas id="line-chartjs" height="225"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="clearfix"></div>

                <div class="col-xs-12 col-lg-4">
                    <section class="box ">
                        <header class="panel_header">
                            <h2 class="title pull-left">Used Devices</h2>
                            <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div>
                        </header>
                        <div class="content-body pb10">
                            <div class="row">
                                <div class="col-xs-8 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 mb-20">
                                    <canvas id="donut-chartjs" width="400" height="400"></canvas>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="token-info">
                                        <div class="info-wrapper three">
                                            <div class="token-descr">
                                                <h3 class="bold mt-0 mb-0">44%</h3>
                                                Desktop
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="token-info">
                                        <div class="info-wrapper five">
                                            <div class="token-descr">
                                                <h3 class="bold mt-0 mb-0">34%</h3>
                                                Mobile
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="token-info">
                                        <div class="info-wrapper two">
                                            <div class="token-descr">
                                                <h3 class="bold mt-0 mb-0">22%</h3>
                                                Tablets
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 col-xs-12">
                                    <div class="token-info">
                                        <div class="info-wrapper default">
                                            <div class="token-descr">
                                                <h3 class="bold mt-0 mb-0">14%</h3>
                                                Others
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-lg-4 col-xs-12">
                    <section class="box has-border-left-3">
                        <header class="panel_header" style="border-bottom:1px solid #eee">
                            <h2 class="title pull-left"><img src="{{ asset('data/app-images/set1.png') }}" class="wd mr-5" alt="">Todo List</h2>
                            <div class="actions panel_actions pull-right">
                                <div class="form-group no-mb">
                                    <button type="submit" class="btn btn-primary btn-corner "><i class="fa fa-check"></i> Update List</button>
                                </div>
                            </div>
                        </header>
                        <div class="content-body">    
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="text-center mt-10 no-mb">
                                        <div class="col-xs-12 no-pl no-pr">
                                            <div class="col-xs-1 no-pl no-pr">
                                                <div style="position:relative;padding: 7px 0 0;">
                                                    <input tabindex="5" type="checkbox"  class="skin-flat-blue" checked>
                                                </div>
                                            </div>
                                            <div class="col-xs-11 no-pl" style="position:relative;padding: 0;">
                                                <h4 class="icheck-label ml-10 text-left form-label"><small>Build an angular app</small></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center no-mt no-mb">
                                        <div class="col-xs-12 no-pl no-pr">
                                            <div class="col-xs-1 no-pl no-pr">
                                                <div style="position:relative;padding: 7px 0 0;">
                                                    <input tabindex="5" type="checkbox"  class="skin-flat-blue">
                                                </div>
                                            </div>
                                            <div class="col-xs-11 no-pl" style="position:relative;padding: 0;">
                                                <h4 class="icheck-label ml-10 text-left form-label"><small>Creating component page</small></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center no-mt no-mb">
                                        <div class="col-xs-12 no-pl no-pr">
                                            <div class="col-xs-1 no-pl no-pr">
                                                <div style="position:relative;padding: 7px 0 0;">
                                                    <input tabindex="5" type="checkbox"  class="skin-flat-blue" checked>
                                                </div>
                                            </div>
                                            <div class="col-xs-11 no-pl" style="position:relative;padding: 0;">
                                                <h4 class="icheck-label ml-10 text-left form-label"><small>Follow back those who follow you</small></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center no-mt no-mb">
                                        <div class="col-xs-12 no-pl no-pr">
                                            <div class="col-xs-1 no-pl no-pr">
                                                <div style="position:relative;padding: 7px 0 0;">
                                                    <input tabindex="5" type="checkbox"  class="skin-flat-blue" checked>
                                                </div>
                                            </div>
                                            <div class="col-xs-11 no-pl" style="position:relative;padding: 0;">
                                                <h4 class="icheck-label ml-10 text-left form-label"><small>Design One page theme</small></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center no-mt no-mb">
                                        <div class="col-xs-12 no-pl no-pr">
                                            <div class="col-xs-1 no-pl no-pr">
                                                <div style="position:relative;padding: 7px 0 0;">
                                                    <input tabindex="5" type="checkbox"  class="skin-flat-blue">
                                                </div>
                                            </div>
                                            <div class="col-xs-11 no-pl" style="position:relative;padding: 0;">
                                                <h4 class="icheck-label ml-10 text-left form-label"><small>Creating component page</small></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center no-mt no-mb">
                                        <div class="col-xs-12 no-pl no-pr">
                                            <div class="col-xs-1 no-pl no-pr">
                                                <div style="position:relative;padding: 7px 0 0;">
                                                    <input tabindex="5" type="checkbox"  class="skin-flat-blue" checked>
                                                </div>
                                            </div>
                                            <div class="col-xs-11 no-pl" style="position:relative;padding: 0;">
                                                <h4 class="icheck-label ml-10 text-left form-label"><small>Creating component page</small></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center no-mt no-mb">
                                        <div class="col-xs-12 no-pl no-pr">
                                            <div class="col-xs-1 no-pl no-pr">
                                                <div style="position:relative;padding: 7px 0 0;">
                                                    <input tabindex="5" type="checkbox"  class="skin-flat-blue">
                                                </div>
                                            </div>
                                            <div class="col-xs-11 no-pl" style="position:relative;padding: 0;">
                                                <h4 class="icheck-label ml-10 text-left form-label"><small>Follow back those who follow you</small></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center no-mt no-mb">
                                        <div class="col-xs-12 no-pl no-pr">
                                            <div class="col-xs-1 no-pl no-pr">
                                                <div style="position:relative;padding: 7px 0 0;">
                                                    <input tabindex="5" type="checkbox"  class="skin-flat-blue">
                                                </div>
                                            </div>
                                            <div class="col-xs-11 no-pl" style="position:relative;padding: 0;">
                                                <h4 class="icheck-label ml-10 text-left form-label"><small>Design One page theme</small></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center mt-10 no-mb">
                                        <div class="col-xs-12 no-pl no-pr">
                                            <div class="col-xs-1 no-pl no-pr">
                                                <div style="position:relative;padding: 7px 0 0;">
                                                    <input tabindex="5" type="checkbox"  class="skin-flat-blue" checked>
                                                </div>
                                            </div>
                                            <div class="col-xs-11 no-pl" style="position:relative;padding: 0;">
                                                <h4 class="icheck-label ml-10 text-left form-label"><small>Build an angular app</small></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center mt-10 no-mb">
                                        <div class="col-xs-12 no-pl no-pr">
                                            <div class="col-xs-1 no-pl no-pr">
                                                <div style="position:relative;padding: 7px 0 0;">
                                                    <input tabindex="5" type="checkbox"  class="skin-flat-blue" checked>
                                                </div>
                                            </div>
                                            <div class="col-xs-11 no-pl" style="position:relative;padding: 0;">
                                                <h4 class="icheck-label ml-10 text-left form-label"><small>Build an angular app</small></h4>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                               
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-lg-4 col-xs-12">
                    <section class="box has-border-left-3">
                        <header class="panel_header" style="border-bottom:1px solid #eee">
                            <h2 class="title pull-left"><img src="{{ asset('data/app-images/set2.png') }}" class="wd mr-5" alt="">Browser Statistics</h2>
                            
                        </header>
                        <div class="content-body">    
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="browser table browser mt-10 mb-0 no-border">
                                        <tbody>
                                            <tr>
                                                <td style="width:40px"><img src="{{ asset('data/app-images/chrome-logo.png') }}" alt="logo"></td>
                                                <td>Google Chrome</td>
                                                <td class="text-right">33%</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('data/app-images/firefox-logo.png') }}" alt="logo"></td>
                                                <td>Mozila Firefox</td>
                                                <td class="text-right">19%</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('data/app-images/safari-logo.png') }}" alt="logo"></td>
                                                <td>Apple Safari</td>
                                                <td class="text-right">09%</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('data/app-images/opera-logo.png') }}" alt="logo"></td>
                                                <td>Opera mini</td>
                                                <td class="text-right">27%</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('data/app-images/netscape-logo.png') }}" alt="logo"></td>
                                                <td>Netscape Navigator</td>
                                                <td class="text-right">05%</td>
                                            </tr>
                                            <tr>
                                                <td><img src="{{ asset('data/app-images/internet-logo.png') }}" alt="logo"></td>
                                                <td>Internet Explorer</td>
                                                <td class="text-right">03%</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>

                               
                            </div>
                        </div>
                    </section>
                    <div class="row">
                       <div class="col-xs-12 mb-15">
                            <div class="reminder-wrapper has-shadow2">
                               <div class="reminder-icon">
                                   <img src="{{ asset('data/app-images/clock.png') }}" width="60" alt="">
                               </div>
                               <div class="reminder-content">
                                   <h4 class="w-text bold">Reminder Alarm</h4>
                                   <h5 class="g-text">Update App version</h5>
                               </div>
                            </div>
                        </div> 
                    </div>
                    
                </div>


                <div class="clearfix"></div>

                <div class="col-lg-12">
                    <section class="box">
                        <header class="panel_header">
                            <h2 class="title pull-left">Recent Orders Queue</h2>
                            <div class="actions panel_actions pull-right">
                                <a class="box_toggle fa fa-chevron-down"></a>
                                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                                <a class="box_close fa fa-times"></a>
                            </div>
                        </header>
                        <div class="content-body">
                            <div class="row">
                                <div class="col-xs-12">

                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Order No.</th>
                                                    <th>Customer</th>
                                                    <th>Status</th>
                                                    <th>Ratings</th>
                                                    <th>Quantity</th>
                                                    <th>Bills</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="round img2">
                                                            <span class="colored-block gradient-blue"></span>
                                                        </div>
                                                        <div class="designer-info">
                                                            <h6>Mobile Game</h6>
                                                            <small class="text-muted">Swift, Python, Java SDK</small>
                                                        </div>
                                                    </td>
                                                    <td>#45202</td>
                                                    <td>Fady solima</td>
                                                    <td><span class="badge badge-md w-70 round-success">Paid</span></td>
                                                    <td class="doc-rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <span>4.8</span>
                                                    </td>
                                                    <td class="text-center">7</td>
                                                    <td>$320</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="round img2">
                                                            <span class="colored-block gradient-pink"></span>
                                                        </div>
                                                        <div class="designer-info">
                                                            <h6>B2B2C Solutions</h6>
                                                            <small class="text-muted">ASP.NET, Ruby, Python</small>
                                                        </div>
                                                    </td>
                                                    <td>#45204</td>
                                                    <td>Gandy Alien</td>
                                                    <td><span class="badge badge-md w-70 round-success">Paid</span></td>
                                                    <td class="doc-rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <span>4.8</span>
                                                    </td>
                                                    <td class="text-center">10</td>
                                                    <td>$170</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="round img2">
                                                            <span class="colored-block gradient-green"></span>
                                                        </div>
                                                        <div class="designer-info">
                                                            <h6>HTML Templates</h6>
                                                            <small class="text-muted">HTML, CSS, JS</small>
                                                        </div>
                                                    </td>
                                                    <td>#45459</td>
                                                    <td>Junior Smith</td>
                                                    <td><span class="badge badge-md w-70 round-danger">Failed</span></td>
                                                    <td class="doc-rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <span>4.8</span>
                                                    </td>
                                                    <td class="text-center">5</td>
                                                    <td>$86</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="round img2">
                                                            <span class="colored-block gradient-violet"></span>
                                                        </div>
                                                        <div class="designer-info">
                                                            <h6>Back-end Plugins</h6>
                                                            <small class="text-muted">PHP, Ruby, C#, ASP.NET</small>
                                                        </div>
                                                    </td>
                                                    <td>#45206</td>
                                                    <td>Flaion fanzzy</td>
                                                    <td><span class="badge badge-md w-70 round-warning">Pending</span></td>
                                                    <td class="doc-rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <span>4.8</span>
                                                    </td>
                                                    <td class="text-center">19</td>
                                                    <td>$433</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="round img2">
                                                            <span class="colored-block gradient-orange"></span>
                                                        </div>
                                                        <div class="designer-info">
                                                            <h6>Admin Dashboards</h6>
                                                            <small class="text-muted">Bootsrap, CSS, SCSS, AngularJS</small>
                                                        </div>
                                                    </td>
                                                    <td> #45406</td>
                                                    <td> Light Ads</td>
                                                    <td><span class="badge badge-md w-70 round-success">Paid</span></td>
                                                    <td class="doc-rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <span>4.8</span>
                                                    </td>
                                                    <td class="text-center">10</td>
                                                    <td>$162</td>
                                                </tr>
                                                

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>
                        
                </div>

                <div class="clearfix"></div>


@endsection