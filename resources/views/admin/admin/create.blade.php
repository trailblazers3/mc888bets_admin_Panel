@extends('layouts.admin')

@section('content')     

<div class="col-xs-12">
    <section class="box ">
        <header class="panel_header">
            <h2 class="title pull-left">Admin Form</h2>
            <div class="actions panel_actions pull-right">
                <a class="box_toggle fa fa-chevron-down"></a>
                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                <a class="box_close fa fa-times"></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-md-8 col-sm-9 col-xs-10">

                    <form>
                        <div class="form-group">
                            <label class="form-label" for="email-1">Email address:</label>
                            <input type="email" class="form-control" id="email-1" placeholder="Enter your email…">
                        </div>

                        <div class="form-group">
                            <label class="form-label" for="password-1">Password:</label>
                            <input type="password" class="form-control" id="password-1" placeholder="Enter your password">
                        </div>

                        <div class="form-group">
                            <label class="form-label">
                                <input type="checkbox" class="iCheck" checked> <span>Remember me</span>
                            </label>
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-primary ">Sign in</button>
                            <button type="button" class="btn btn-purple  pull-right">Register now</button>
                        </div>

                    </form>

                </div>
            </div>

        </div>
    </section>
</div>

@endsection