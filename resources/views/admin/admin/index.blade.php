@extends('layouts.admin')

@section('content')

<div class="col-lg-12">
    <section class="box">
        <header class="panel_header">
            <h2 class="title pull-left">Admin Table List</h2>
            <div class="actions panel_actions pull-right">
                <a class="box_toggle fa fa-chevron-down"></a>
                <a class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></a>
                <a class="box_close fa fa-times"></a>
            </div>
        </header>
        <div class="content-body">
            <div class="row">
                <div class="col-xs-12">

                    <div class="table-responsive" data-pattern="priority-columns">
                        <table id="tech-companies-1" class="table vm table-small-font no-mb table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Quantity</th>
                                    <th>Remark</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="round img2">
                                            <span class="colored-block gradient-blue"></span>
                                        </div>
                                        <div class="designer-info">
                                            <h6>Mr. Yu Wan</h6>
                                            <small class="text-muted">No.123, Road 43x43, Mandalay</small>
                                        </div>
                                    </td>
                                    <td>User/Agent/Refree</td>
                                    <td>Status is Something</td>
                                    <td><span class="badge badge-md w-70 round-success">Paid</span></td>
                                    <td>this is Remark</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="round img2">
                                            <span class="colored-block gradient-pink"></span>
                                        </div>
                                        <div class="designer-info">
                                            <h6>Mr. Yu Wan</h6>
                                            <small class="text-muted">No.123, Road 43x43, Mandalay</small>
                                        </div>
                                    </td>
                                    <td>User/Agent/Refree</td>
                                    <td>Status is Something</td>
                                    <td><span class="badge badge-md w-70 round-success">Paid</span></td>
                                    <td>this is Remark</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="round img2">
                                            <span class="colored-block gradient-green"></span>
                                        </div>
                                        <div class="designer-info">
                                            <h6>Mr. Yu Wan</h6>
                                            <small class="text-muted">No.123, Road 43x43, Mandalay</small>
                                        </div>
                                    </td>
                                    <td>User/Agent/Refree</td>
                                    <td>Status is Something</td>
                                    <td><span class="badge badge-md w-70 round-success">Paid</span></td>
                                    <td>this is Remark</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="round img2">
                                            <span class="colored-block gradient-violet"></span>
                                        </div>
                                        <div class="designer-info">
                                            <h6>Mr. Yu Wan</h6>
                                            <small class="text-muted">No.123, Road 43x43, Mandalay</small>
                                        </div>
                                    </td>
                                    <td>User/Agent/Refree</td>
                                    <td>Status is Something</td>
                                    <td><span class="badge badge-md w-70 round-success">Paid</span></td>
                                    <td>this is Remark</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="round img2">
                                            <span class="colored-block gradient-orange"></span>
                                        </div>
                                        <div class="designer-info">
                                            <h6>Mr. Yu Wan</h6>
                                            <small class="text-muted">No.123, Road 43x43, Mandalay</small>
                                        </div>
                                    </td>
                                    <td>User/Agent/Refree</td>
                                    <td>Status is Something</td>
                                    <td><span class="badge badge-md w-70 round-success">Paid</span></td>
                                    <td>this is Remark</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="round img2">
                                            <span class="colored-block gradient-blue"></span>
                                        </div>
                                        <div class="designer-info">
                                            <h6>Mr. Yu Wan</h6>
                                            <small class="text-muted">No.123, Road 43x43, Mandalay</small>
                                        </div>
                                    </td>
                                    <td>User/Agent/Refree</td>
                                    <td>Status is Something</td>
                                    <td><span class="badge badge-md w-70 round-success">Paid</span></td>
                                    <td>this is Remark</td>
                                </tr>
                                

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>
        
</div>

<div class="clearfix"></div>

@endsection